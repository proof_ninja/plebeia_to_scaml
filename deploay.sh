#!/bin/sh
set -eux
DATE=`date +"%Y%m%d_%I%M"`
DIR=`pwd $(dirname $0)`
SCAMLC=~/gitlab/dailambda/scaml/_opam/bin/scamlc
TZ=$DIR/merkle_$DATE.tz
$SCAMLC smart_contract/merkle.ml
mv merkle.tz $TZ
cd ~/tezos-testnet/
./tezos-client originate contract merkle_$DATE transferring 0 from faucet running $TZ --burn-cap 10 --init 'Unit'
