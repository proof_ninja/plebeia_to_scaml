module C = Plebeia_to_scaml.Converter

let plebeia =
  let open Plebeia in
  let open Node_type in
  let v = Value.of_string "VALUEDAYO" in
  let seg = Segment.unfat [`Left; `Left; `Left; `Right] in (*適当*)
  let h = Hash.of_prefix (Hash.Prefix.of_string "0123") in
  let left = new_leaf v in
  let right = new_bud (Some (new_extender seg (Hash h))) in
  new_bud (Some (new_internal left right))


let () =
(*  let m = C.to_michelson D.node in*)
  let node = C.of_plebeia_node plebeia in
  let m = C.to_michelson node in
  print_endline m
