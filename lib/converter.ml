open Smart_contract
module N = Plebeia.Node_type

type hash =  Plebeia.Hash.t

module D = Smart_contract.Merkle

let key_state = ref 0
let gen_key () =
  let r = !key_state in
  key_state := succ r; SCaml.Nat r

let bytes_of_hex_string s =
  SCaml.Bytes ("0x" ^ s)

let bytes_of_string s =
  let `Hex h = Hex.of_string s in
  bytes_of_hex_string h

let value_of_plebeia_value v =
  bytes_of_hex_string (Plebeia.Value.to_hex_string v)

let encoded_segment_of_plebeia seg =
  bytes_of_string (Plebeia.Segment.Serialization.encode seg)

let hash_of_plebeia (hash : Plebeia.Hash.t) : D.hash =
  match Plebeia.Hash.to_strings hash with
      | [prefix] -> (bytes_of_string prefix, Bytes "0x")
      | [prefix; s] -> (bytes_of_string prefix, bytes_of_string s)
      | _ -> failwith "hash_of_plebeia"


let of_plebeia_node node =
  let rec of_node store key = function
    | N.Disk _ -> failwith "of_plebeia_node: Disk"
    | N.View v ->
      of_view store key v
    | N.Hash h -> (key, D.Hash (hash_of_plebeia h)) :: store
  and of_view store key = function
    | N.Internal (l, r, _, _) ->
      let keyl = gen_key () in
      let keyr = gen_key () in
      (key, D.Internal (keyl, keyr)) :: store
      |> (fun store -> of_node store keyl l)
      |> (fun store -> of_node store keyr r)
    | N.Bud (None, _, _) ->
      (key, Bud None) :: store
    | N.Bud (Some n, _, _) ->
      let key_child = gen_key () in
      of_node ((key, Bud (Some key_child)) :: store) key_child n
    | Leaf (v, _, _) ->
      (key, Leaf (value_of_plebeia_value v)) :: store
    | N.Extender (seg, n, _, _) ->
      let key_child = gen_key () in
      let eseg = encoded_segment_of_plebeia seg in
      of_node ((key, Extender (eseg, key_child)) :: store) key_child n
  in
  let root_key = gen_key () in
  of_node [] root_key node

let to_michelson (t: D.node) =
  let list_to_m f xs =
    Printf.sprintf "{%s}" (String.concat "; " @@ List.map f xs)
  in
  let nat_to_m (SCaml.Nat n) =
    Printf.sprintf "%d" n
  in
  let key_to_m = nat_to_m in
  let bytes_to_m (SCaml.Bytes s) = s in
  let pair_to_m f g (x, y) =
    Printf.sprintf "Pair(%s, %s)" (f x) (g y)
  in
  let option_to_m f = function
    | None -> "None"
    | Some x -> Printf.sprintf "Some (%s)" (f x)
  in
  let node_ref_to_m = function
    | D.Hash (x,y) -> Printf.sprintf "Left (Left (Pair %s %s))"
                      (bytes_to_m x) (bytes_to_m y)
    | Bud no -> Printf.sprintf "Left (Right (%s))" (option_to_m key_to_m no)
    | Leaf v -> Printf.sprintf "Right (Left (%s))" (bytes_to_m v)
    | Internal (l, r) ->
      Printf.sprintf "Right (Right (Left (Pair %s %s)))"
        (key_to_m l) (key_to_m r)
    | Extender (seg, n) ->
      Printf.sprintf "Right (Right (Right (Pair %s %s)))"
        (bytes_to_m seg) (key_to_m n)
  in
  list_to_m (pair_to_m key_to_m node_ref_to_m) t
